require 'scss_lint/rake_task'
require 'yaml'
require 'yaml-lint'
require 'stringex'

desc 'Run all lint tasks'
task lint: ['lint:scss', 'lint:yaml', 'lint:features:solutions', 'lint:docs_ee'] do
end

namespace :lint do
  desc 'Lint SCSS files'
  task :scss do
    SCSSLint::RakeTask.new
    Rake::Task['scss_lint'].invoke
  end

  desc 'Lint YAML files'
  task :yaml do
    failed = 0
    Dir['data/**/*.yml'].each do |yml|
      lint = YamlLint.new(yml)
      failed += lint.do_lint
    end

    exit failed unless failed.zero?
  end

  namespace :features do
    desc 'Ensure every feature has a solution'
    task :solutions do
      failed = 0

      puts 'Checking if a feature is missing a solution...'
      puts '----------------------------'

      file = YAML.load_file('data/features.yml')
      file['features'].each do |feature|
        if feature['solution'].nil?
          puts "'#{feature['title']}'"
          failed += 1
        end
      end

      unless failed.zero?
        puts '----------------------------'
        if failed == 1
          puts "Oops! #{failed} solution is missing :( Read how to fix this:"
        else
          puts "Oops! #{failed} solutions are missing :( Read how to fix this:"
        end
        puts 'https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/features.md#create-or-update-the-solutions-pages-under-solutions'
        exit 1
      end

      puts 'Every feature has a solution! Congrats!'
    end
  end

  desc "Check that all docs point to /ee/"
  task :docs_ee do
    abort unless system('./scripts/docs_ee_check.sh')
  end
end

desc 'Begin a new post'
task :new_post, :title do |t, args|
  if args.title
    title = args.title
  else
    puts 'Enter a title for your post: '
    title = STDIN.gets.chomp
  end

  filename = "source/posts/#{Time.now.strftime('%Y-%m-%d')}-#{title.to_url}.html.md"
  puts "Creating new post: #{filename}"
  open(filename, 'w') do |post|
    post.puts '---'
    post.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    post.puts 'author: '
    post.puts 'author_gitlab: '
    post.puts 'author_twitter: '
    post.puts 'categories: '
    post.puts 'image_title: '
    post.puts 'description: '
    post.puts '---'
  end
end

namespace :release do
  desc 'Creates a new release post for major and minor versions'
  task :monthly, :version do |t, args|
    version = args.version
    source_dir = File.expand_path('../source', __FILE__)
    date = Time.now.strftime('%Y-%m-22')
    posts_dir = 'posts'

    raise 'You need to specify a minor version, like 10.1' unless version =~ /\A\d+\.\d+\z/

    md_version = version.tr('.', '-')
    md_filename = "#{source_dir}/#{posts_dir}/#{date}-gitlab-#{md_version}-released.html.md"

    if File.exist?(md_filename)
      abort('rake aborted!') if ask("#{md_filename} already exists. Do you want to overwrite?", %w[y n]) == 'n'
    end

    puts "Creating new release post: #{md_filename}"

    md_text = File.read('doc/templates/blog/monthly_release_blog_template.html.md')
    md_text.gsub!('X_X', version.tr('.', '_'))
    md_text.gsub!('X.X', version)
    md_text.gsub!('X-X', version.tr('.', '-'))

    open(md_filename, 'w') do |post|
      post.puts md_text
    end

    yaml_date = date.tr('-', '_')
    yaml_version = version.tr('.', '_')
    yaml_filename = "data/release_posts/#{yaml_date}_gitlab_#{yaml_version}_released.yml"

    if File.exist?(yaml_filename)
      abort('rake aborted!') if ask("#{yaml_filename} already exists. Do you want to overwrite?", %w[y n]) == 'n'
    end

    puts "Creating new release post yaml: #{yaml_filename}"

    yaml_text = File.read('doc/templates/blog/YYYY_MM_DD_gitlab_x_y_released.yml')
    yaml_text.gsub!('X_X', version.tr('.', '_'))
    yaml_text.gsub!('X.X', version)
    yaml_text.gsub!('X-X', version.tr('.', '-'))

    open(yaml_filename, 'w') do |yaml|
      yaml.puts yaml_text
    end
  end

  # Do not use this task for major or minor releases that go out on 22nd
  desc 'Creates a new release post for patch versions'
  task :patch, :version do |t, args|
    version = args.version
    source_dir = File.expand_path('../source', __FILE__)
    posts_dir = 'posts'

    raise 'You need to specify a patch version, like 10.1.1' unless version =~ /\A\d+\.\d+\.\d+\z/

    version = version.tr('.', '-')
    date = Time.now.strftime('%Y-%m-%d')
    filename = "#{source_dir}/#{posts_dir}/#{date}-gitlab-#{version}-released.html.md"

    if File.exist?(filename)
      abort('rake aborted!') if ask("#{filename} already exists. Do you want to overwrite?", %w[y n]) == 'n'
    end

    puts "Creating new release post: #{filename}"

    template_text = File.read('doc/templates/blog/patch_release_blog_template.html.md')
    template_text.gsub!('X_X', version.tr('.', '_'))
    template_text.gsub!('X.X', version)
    template_text.gsub!('X-X', version.tr('.', '-'))

    open(filename, 'w') do |post|
      post.puts template_text
    end
  end
end

desc 'Create a new press release'
task :new_press, :title do |t, args|
  data_dir = File.expand_path('../data', __FILE__)

  puts 'Enter a date for the press release (ISO format, example: 2016-12-30): '
  date = STDIN.gets.chomp
  puts 'Enter a title for the press release: '
  title = STDIN.gets.chomp

  filename = "source/press/releases/#{date}-#{title.to_url}.html.md"
  puts "Creating new press release: #{filename}"
  open(filename, 'w') do |pressrel|
    pressrel.puts '---'
    pressrel.puts 'layout: markdown_page'
    pressrel.puts "title: \"#{title.gsub(/&/, '&amp;')}\""
    pressrel.puts '---'
    pressrel.puts ''
  end

  press_yml = "#{data_dir}/press.yml"
  puts 'Populating data/press.yml'
  open(press_yml, 'a') do |yaml|
    yaml.puts ''
    yaml.puts "- title: \"#{title.gsub(/&/, '&amp;')}\""
    yaml.puts "  link: #{date}-#{title.to_url}.html"
    yaml.puts "  date: #{date}"
  end
end

desc 'Add an existing press release to the archive'
task :add_press, :title do |t, args|
  data_dir = File.expand_path('../data', __FILE__)

  puts 'Enter a date for the press release (ISO format, example: 2016-12-30): '
  date = STDIN.gets.chomp
  puts 'Enter a title for the press release: '
  title = STDIN.gets.chomp
  puts 'Enter the URL of the press release: '
  link = STDIN.gets.chomp

  press_yml = "#{data_dir}/press.yml"
  puts 'Populating data/press.yml'
  open(press_yml, 'a') do |yaml|
    yaml.puts ''
    yaml.puts "- title: \"#{title}\""
    yaml.puts "  link: #{link}"
    yaml.puts "  date: #{date}"
  end
end

desc 'Build the site in public/ (for deployment)'
task :build do
  build_cmd = %w[middleman build]
  raise "command failed: #{build_cmd.join(' ')}" unless system(*build_cmd)
end

PDFS = %w[
  public/terms/print/githost_terms.pdf
  public/terms/print/gitlab_com_terms.pdf
  public/terms/print/gitlab_consultancy_terms.pdf
  public/terms/print/gitlab_subscription_terms.pdf
  public/terms/print/gitlab_subscription_terms_sig.pdf
  public/high-availability/gitlab-ha.pdf
  public/pdfs/the-eleven-rules-of-gitlab-flow.pdf
].freeze

PDF_TEMPLATE = 'pdf_template.tex'.freeze

# public/foo/bar.pdf depends on public/foo/bar.html
rule %r{^public/.*\.pdf} => [->(f) { f.pathmap('%X.html') }, PDF_TEMPLATE] do |pdf|
  # Avoid distracting 'newline appended' message
  open(pdf.source, 'a', &:puts)
  # Rewrite the generated HTML to fix image links for pandoc. Image paths
  # need to be relative paths starting with 'public/'.
  IO.popen(%W[ed -s #{pdf.source}], 'w') do |ed|
    ed.puts <<~'REGEX'
      H
      g/\.\.\/images\// s//\/images\//g
      g/'\/images\/ s//'public\/images\//g
      g/"\/images\// s//"public\/images\//g
      wq
    REGEX
  end
  options = %W[--template=#{PDF_TEMPLATE} --latex-engine=xelatex -V date=#{Time.now}]
  warn "Generating #{pdf.name}"
  cmd = ['pandoc', *options, '-o', pdf.name, pdf.source]
  abort("command failed: #{cmd.join(' ')}") unless system(*cmd)
end

desc 'Generate PDFs'
task pdfs: PDFS

desc 'Remove PDFs'
task :rm_pdfs do
  PDFS.each do |pdf|
    if File.exist? pdf
      File.delete pdf
      puts "Deleting #{pdf}"
    end
  end
end

desc 'Comparison PDFS'
task :comparison_pdfs do
  file = YAML.load_file('data/features.yml')
  file['comparisons'].each do |key, comparison|
    file_name = "public/comparison/pdfs/#{key.dup.tr(/_/, '-')}.html"
    pdf_file_name = "source/comparison/pdfs#{comparison['link'].dup.gsub(/html/, 'pdf').gsub(%r{comparison/}, '')}"

    abort('Error generating comparison PDFs 😔') unless system("./comparison_pdfs.sh #{file_name} #{pdf_file_name}")
  end
end
