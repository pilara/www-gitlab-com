---
layout: job_page
title: "Federal Strategic Account Leader"
---

This has been moved to [/roles/federal-strategic-account-leader](https://about.gitlab.com/roles/federal-strategic-account-leader/).
